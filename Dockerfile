FROM python

ADD . /app

COPY . /app

WORKDIR /app

RUN pip install flask

CMD ["python", "api-asincrona.py"]
