from flask import Flask
app = Flask(__name__)
rota = 0
import os
import platform
import socket


@app.route('/')
def hello_world():
    return "ola mundo, este webserver está rodando na plataforma : " + str(platform.node()) 

@app.route('/erik/')
def hello_erik():
    return "ola Erik"

@app.route('/fila/')
def fila():
    global rota
    rota +=1
    return "ola, bem vindo a fila, seu numero é: " + str(rota)

@app.route('/<rota>')
def resultado(rota):
    return "ola, bem vindo a sua rota, o seu resultado é: " + str(rota)


if __name__ == "__main__":
    app.run(debug=True,port="5000",host="0.0.0.0")
